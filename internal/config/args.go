package config

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

// Args contains the command line arguments
type Args struct {
	Address     string
	Debug       bool
	GitLabToken string
	GitLabURL   string
	GroupIDs    []int
	groupIDs    string
	Labels      []string
	MetricsPath string
	ProjectIDs  []int
	projectIDs  string
	Recursive   bool
}

// ParseArgs parses the command line arguments
func ParseArgs() Args {
	var args Args

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&args.Recursive, "recursive", false, "scan groups recursively")
	flag.StringVar(&args.Address, "address", ":9604", "listening address")
	flag.StringVar(&args.MetricsPath, "metrics", "/metrics", "metrics path")
	flag.StringVar(&args.GitLabURL, "gitlab-url", "", "the url of the GitLab instance")
	flag.StringVar(&args.groupIDs, "group-ids", "", "comma separated list of group IDs")
	flag.StringVar(&args.projectIDs, "project-ids", "", "comma separated list of project IDs")

	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Println("\nEnvironment variables")
		fmt.Println("  GITLAB_TOKEN token used to access the GitLab instance")
	}

	flag.Parse()

	args.GitLabToken = os.Getenv("GITLAB_TOKEN")
	args.Labels = strings.Split(os.Getenv("GITLAB_LABELS"), ",")

	if args.GitLabToken == "" || args.GitLabURL == "" {
		log.Fatalf("GitLab token or url not defined")
	}

	args.GroupIDs = parseIDs(args.groupIDs)
	args.ProjectIDs = parseIDs(args.projectIDs)
	return args
}

func parseIDs(ids string) []int {
	if ids == "" {
		return nil
	}
	strs := strings.Split(ids, ",")
	ints := make([]int, len(strs))
	for i := range ints {
		ints[i], _ = strconv.Atoi(strs[i])
	}
	return ints
}
