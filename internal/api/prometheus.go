package api

import (
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/prometheus-exporters/gitlab-issues-exporter/internal/metrics"
)

const (
	namespace = "gitlab_issues"
)

// Describe implements Collector interface.
func (g GitLabClient) Describe(ch chan<- *prometheus.Desc) {
	metrics.BootTime.Describe(ch)
	metrics.ClientErrors.Describe(ch)
	metrics.ClientRequests.Describe(ch)
	metrics.ExporterUp.Describe(ch)
}

// Collect collects GitLab issues metrics
func (g GitLabClient) Collect(ch chan<- prometheus.Metric) {
	metrics.ExporterUp.Set(1)
	now := time.Now()

	var wg sync.WaitGroup
	wg.Add(len(g.ProjectIDs))
	for _, projectID := range g.ProjectIDs {
		go func(projectID int, now time.Time) {
			defer wg.Done()

			project, _, err := g.Client.Projects.GetProject(projectID, g.GetProjectOptions)
			if err != nil {
				log.Printf("Failed to fetch project name for project ID %d: %s", projectID, err)
				return
			}

			issues, err := g.GetIssuesByProject(projectID)
			if err != nil {
				log.Fatalf("Error fetching issues for project %s: %s", project.PathWithNamespace, err)
			}

			openedIssues, closedIssues, err := splitIssuesByState(issues)
			if err != nil {
				log.Errorln(project.PathWithNamespace, err)
				return
			}

			metrics.Issues.WithLabelValues(project.PathWithNamespace, "opened", "_all").Set(float64(len(openedIssues)))
			for _, i := range openedIssues {
				age := now.Sub(*i.CreatedAt).Hours()
				metrics.IssueOpeningAgeHistogram.WithLabelValues(project.PathWithNamespace, i.State).Observe(age)

				lastUpdate := now.Sub(*i.UpdatedAt).Hours()
				metrics.IssueLastUpdateAgeHistogram.WithLabelValues(project.PathWithNamespace).Observe(lastUpdate)

			}

			metrics.Issues.WithLabelValues(project.PathWithNamespace, "closed", "_all").Set(float64(len(closedIssues)))
			for _, i := range closedIssues {
				openedAge := now.Sub(*i.CreatedAt).Hours()
				metrics.IssueOpeningAgeHistogram.WithLabelValues(project.PathWithNamespace, i.State).Observe(openedAge)
				// In Gitlab, it is possible that an issue is in closed state
				// but doesn't have a closing timestamp.
				// In database terms, this means closed_at being NULL and
				// state_id = 2 in the issues table.
				if i.ClosedAt != nil {
					closedAge := now.Sub(*i.ClosedAt).Hours()
					metrics.IssueClosingAgeHistogram.WithLabelValues(project.PathWithNamespace).Observe(closedAge)
				}
			}

			for _, label := range g.Labels {
				labelID := strings.ToLower(strings.Replace(label, " ", "-", -1))
				issuesByLabel := getIssuesByLabel(issues, label)
				openedissuesByLabel, closedissuesByLabel, err := splitIssuesByState(issuesByLabel)
				if err != nil {
					log.Errorf("Couldn't split issues for label %s: %s", label, err)
				}
				metrics.Issues.WithLabelValues(project.PathWithNamespace, "opened", labelID).Set(float64(len(openedissuesByLabel)))
				metrics.Issues.WithLabelValues(project.PathWithNamespace, "closed", labelID).Set(float64(len(closedissuesByLabel)))
			}

			if len(openedIssues) > 0 {
				metrics.OldestIssue.WithLabelValues(project.PathWithNamespace).Set(getOldestIssue(openedIssues))
			}
		}(projectID, now)
	}
	wg.Wait()

	metrics.BootTime.Collect(ch)
	metrics.ClientErrors.Collect(ch)
	metrics.ClientRequests.Collect(ch)
	metrics.ExporterUp.Collect(ch)
	metrics.IssueOpeningAgeHistogram.Collect(ch)
	metrics.IssueClosingAgeHistogram.Collect(ch)
	metrics.IssueLastUpdateAgeHistogram.Collect(ch)
	metrics.Issues.Collect(ch)
	metrics.OldestIssue.Collect(ch)

	metrics.IssueOpeningAgeHistogram.Reset()
	metrics.IssueOpeningAgeHistogram.Reset()
	metrics.IssueClosingAgeHistogram.Reset()
	metrics.IssueLastUpdateAgeHistogram.Reset()
}
