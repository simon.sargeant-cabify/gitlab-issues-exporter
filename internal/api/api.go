package api

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/yakshaving.art/prometheus-exporters/gitlab-issues-exporter/internal/config"
	"gitlab.com/yakshaving.art/prometheus-exporters/gitlab-issues-exporter/internal/metrics"
)

// GitLabClient is a GitLab API client with some project options
type GitLabClient struct {
	Client            *gitlab.Client
	GetProjectOptions *gitlab.GetProjectOptions
	ProjectIDs        []int
	Labels            []string
}

// NewClient creates a new API client to a GitLab endpoint
func NewClient(args config.Args) (GitLabClient, error) {
	client := gitlab.NewClient(nil, args.GitLabToken)
	if err := client.SetBaseURL(args.GitLabURL); err != nil {
		log.Fatalf("Could not set base URL to GitLab Client: '%s'", err)
	}
	projectIDs := make([]int, 0)
	for _, groupID := range args.GroupIDs {
		projects, err := GetProjectsByGroup(client, groupID, args.Recursive)
		if err != nil {
			return GitLabClient{}, err
		}
		for _, project := range projects {
			projectIDs = append(projectIDs, project.ID)
		}
	}
	if args.ProjectIDs != nil {
		projectIDs = append(args.ProjectIDs, projectIDs...)
	}
	args.ProjectIDs = uniqueSlice(projectIDs)
	log.Debugf("projects to scan: %v", args.ProjectIDs)

	return GitLabClient{
		Client:            client,
		GetProjectOptions: &gitlab.GetProjectOptions{},
		ProjectIDs:        args.ProjectIDs,
		Labels:            args.Labels,
	}, nil
}

// GetIssuesByProject returns all issues for a GitLab project
func (g GitLabClient) GetIssuesByProject(projectID int) ([]*gitlab.Issue, error) {
	issues := make([]*gitlab.Issue, 0)
	searchOpts := &gitlab.SearchOptions{
		Page:    1,
		PerPage: 100,
	}

	metrics.ClientRequests.Inc()
	issues, resp, err := g.Client.Search.IssuesByProject(projectID, "", searchOpts)
	if err != nil {
		metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
		return nil, err
	}
	if resp.CurrentPage >= resp.TotalPages {
		return issues, nil
	}

	issuesCh := make(chan []*gitlab.Issue)
	errs := make(chan error)
	var wg sync.WaitGroup
	for page := 2; page <= resp.TotalPages; page++ {
		wg.Add(1)
		go func(pid int, page int) {
			defer wg.Done()
			so := &gitlab.SearchOptions{
				Page:    page,
				PerPage: 100,
			}
			metrics.ClientRequests.Inc()
			i, _, err := g.Client.Search.IssuesByProject(pid, "", so)
			if err != nil {
				metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
				errs <- err
				return
			}
			issuesCh <- i
		}(projectID, page)
	}

	go func() {
		wg.Wait()
		close(issuesCh)
	}()

	for i := range issuesCh {
		issues = append(issues, i...)
	}

	select {
	case err := <-errs:
		return nil, err
	default:
		return issues, nil
	}
}

// GetProjectName returns the project name for a given project ID.
func (g GitLabClient) GetProjectName(pid int) (string, error) {
	metrics.ClientRequests.Inc()
	project, _, err := g.Client.Projects.GetProject(pid, &gitlab.GetProjectOptions{})
	if err != nil {
		metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
	}
	return project.Name, err
}

// GetProjectsByGroup returns the list of projects under a group, optionally recursive.
func GetProjectsByGroup(client *gitlab.Client, gid int, recursive bool) ([]*gitlab.Project, error) {
	projects := make([]*gitlab.Project, 0)
	shared := false
	issuesEnabled := true
	listGroupOpts := &gitlab.ListGroupProjectsOptions{
		IncludeSubgroups:  &recursive,
		WithIssuesEnabled: &issuesEnabled,
		WithShared:        &shared,
	}

	metrics.ClientRequests.Inc()
	projects, resp, err := client.Groups.ListGroupProjects(gid, listGroupOpts)
	if err != nil {
		metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
		return nil, err
	}

	if resp.CurrentPage >= resp.TotalPages {
		return (projects), nil
	}

	projectsCh := make(chan []*gitlab.Project)
	errs := make(chan error)
	var wg sync.WaitGroup
	for page := 2; page <= resp.TotalPages; page++ {
		wg.Add(1)
		go func(gid int, page int) {
			defer wg.Done()
			so := &gitlab.ListGroupProjectsOptions{
				ListOptions:       gitlab.ListOptions{Page: page},
				IncludeSubgroups:  &recursive,
				WithIssuesEnabled: &issuesEnabled,
				WithShared:        &shared,
			}
			metrics.ClientRequests.Inc()
			p, _, err := client.Groups.ListGroupProjects(gid, so)
			if err != nil {
				metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
				errs <- err
				return
			}
			projectsCh <- p
		}(gid, page)
	}

	go func() {
		wg.Wait()
		close(projectsCh)
	}()

	for i := range projectsCh {
		projects = append(projects, i...)
	}

	select {
	case err := <-errs:
		return nil, err
	default:
		return projects, nil
	}
}

func splitIssuesByState(allIssues []*gitlab.Issue) ([]*gitlab.Issue, []*gitlab.Issue, error) {
	opened := make([]*gitlab.Issue, 0)
	closed := make([]*gitlab.Issue, 0)
	for _, i := range allIssues {
		switch i.State {
		case "opened":
			opened = append(opened, i)
		case "closed":
			closed = append(closed, i)
		default:
			return nil, nil, fmt.Errorf("invalid issue state: aborting issue splitting")
		}
	}
	return opened, closed, nil
}

func getIssuesByLabel(allIssues []*gitlab.Issue, label string) []*gitlab.Issue {
	res := make([]*gitlab.Issue, 0)
	for _, i := range allIssues {
		for _, l := range i.Labels {
			if l == label {
				res = append(res, i)
			}
		}
	}
	return res
}

func getOldestIssue(allIssues []*gitlab.Issue) float64 {
	res := allIssues[0]
	for _, i := range allIssues {
		if res.CreatedAt.Sub(*i.CreatedAt) > 0 {
			res = i
		}
	}
	return float64(res.CreatedAt.Unix())
}

func uniqueSlice(s []int) []int {
	seen := make(map[int]struct{}, len(s))
	j := 0
	for _, v := range s {
		if _, ok := seen[v]; ok {
			continue
		}
		seen[v] = struct{}{}
		s[j] = v
		j++
	}
	return s[:j]
}
